import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { interval, Subject, takeUntil } from 'rxjs';
import { PageRoutes } from '../routes';

@Component({
  selector: 'app-changes',
  templateUrl: './changes.component.html',
  styleUrls: ['./changes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangesComponent implements OnInit, OnDestroy {

  lastChangeSource: string = 'Нет изменений';
  changesCount: number = 0;

  unsubscribe$ = new Subject();

  constructor(private cd: ChangeDetectorRef, private router: Router) { }

  ngOnInit(): void {
    this.setInterval();
  }

  setInterval(): void {
    interval(10000).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.callChange('Интервал в 10 секунд');
      this.cd.markForCheck();
    });
  }

  callChange(lastChangeSource: string): void {
    this.lastChangeSource = lastChangeSource;
    this.changesCount += 1;
  }

  navigateToFormPage() {
    this.router.navigate([PageRoutes.form]);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(null);
    this.unsubscribe$.complete();
  }

}
