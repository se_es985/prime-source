import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subject, takeUntil } from 'rxjs';
import { PageRoutes } from '../routes';
import { Message } from './message-storage/message';
import { MessageStorageService } from './message-storage/message-storage.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormComponent implements OnInit, OnDestroy {

  messageFormGroup: FormGroup;
  message$: Observable<Message> = this.messageStorageService.message$;

  unsubscribe$ = new Subject();

  constructor(private fb: FormBuilder, private router: Router, private messageStorageService: MessageStorageService) {
    this.messageFormGroup = this.fb.group({
      sender: '',
      text: '',
      recipient: '',
    });
  }

  ngOnInit(): void {
    this.initializeMessage();
  }

  initializeMessage(): void {
    this.messageStorageService.loadMessage();
    this.message$.pipe(takeUntil(this.unsubscribe$)).subscribe((message) => {
      this.messageFormGroup.patchValue(message);
    })
  }

  submitMessage(): void {
    console.log(this.messageFormGroup.value);
    this.messageStorageService.saveMessage(this.messageFormGroup.value);
  }

  clearMessageData(): void {
    this.messageStorageService.clearMessage();
  }

  navigateToChangesPage(): void {
    this.router.navigate([PageRoutes.changes]);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(null);
    this.unsubscribe$.complete();
  }

}
