import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { initialMessage, Message } from './message';

@Injectable({
  providedIn: 'root'
})
export class MessageStorageService {

  private messageSubject$ = new BehaviorSubject<Message>(initialMessage);

  public message$: Observable<Message> = this.messageSubject$.asObservable();

  public loadMessage(): void {
    const jsonData = localStorage.getItem('message');
    if (jsonData) {
      const message: Message = JSON.parse(jsonData) as Message;
      this.messageSubject$.next(message);
    }
  }

  public saveMessage(message: Message): void {
    const jsonData = JSON.stringify(message);
    localStorage.setItem('message', jsonData);
    this.messageSubject$.next(message);
  }

  public clearMessage(): void {
    localStorage.clear();
    this.messageSubject$.next(initialMessage);
  }
}
