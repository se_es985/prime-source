export interface Message {
  sender: string,
  text: string,
  recipient: string,
}

export const initialMessage: Message = {
  sender: '',
  text: '',
  recipient: '',
}
