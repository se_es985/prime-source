import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangesComponent } from './pages/changes/changes.component';
import { PageRoutes } from './pages/routes';

const routes: Routes = [
  {
    path: PageRoutes.changes,
    component: ChangesComponent,
  },
  {
    path: PageRoutes.form,
    loadChildren: () => import('./pages/form/form.module').then((m) => m.FormModule),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: PageRoutes.changes,
  },
  {
    path: '**',
    redirectTo: PageRoutes.changes,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
